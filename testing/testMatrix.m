% Finite element matrices obtained symbolically

syms L xi x E rho I A

% Interpolation functions
% Defined elementwise in the domain [-1 1]

N1 = 1/4*(1-xi)^2*(2+xi);
N2 = L/8*(1-xi)^2*(1+xi);
N3 = 1/4*(1+xi)^2*(2-xi);
N4 = L/8*(1+xi)^2*(xi-1);

Ni = [N1 N2 N3 N4];

xi = 2*x/L -1; 

% Derivatives
% Chain rule

Nixx = diff(Ni,2)*jacobian(xi,x)^2;

% Elementary matrices
% L/2 for interval changing

% Stiffness matrix
K_N = Nixx.'*Nixx;
ke = E*I*L/2*int(K_N,-1,1);

% Mass matrix
M_N = Ni.'*Ni;
me = rho*A*L/2*int(M_N,-1,1);

% Force vector 
% TODO: check
F_N = Ni;
fe = L/2*int(F_N,-1,1);