% Computation of response of FLD beam
% -----------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;    % number of nodes
dof = 2;   % number of dof/node

% Material and section data

b = 0.01;         % width of section
Lt = 0.12;        % length of beam
rho = 7782;       % density 
E = 176.2E9;      % Young's modulus
nu = 0.3;         % Poisson's ratio
G = E/(2*(1+nu)); % shear modulus

L = Lt/(N-1);     % element length

% Different materials
% -------------------

% The 1 indicates viscoelastic material
h = 0.002;
h1   = 0.002;
% h1   = 0.006;
% h1   = 0.010;
rho1 = 1423; 

A = b*h;
A1   = b*h1;
At   = b*(h+h1);

rhoL = rho*A + rho1*A1;

% Fractional damping
% ------------------

syms f

E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

E1 = (E_0 + E_inf*(1i*tau*f).^alpha)./(1+(1i*tau*f).^alpha);
G1 = E1/(2*(1+nu)); 

hn = (E*h^2 + E1*h1*(2*h + h1))./(2*(E*h+E1*h1)); % neutral fiber
I = 1/12*b*h^3 + b*h*(hn - h/2).^2; % inertia moment of base material
I1 = 1/12*b*h1^3 + b*h1*(h+h1/2-hn).^2; % inertia moment of viscoelastic material 

Beq(f) = E1*I1 + E*I;

syms y

K  = 1/(E^2*b/(4*G*Beq^2)*int((hn^2-y^2)^2,[-hn -hn+h]));
K1 = 1/(E1^2*b/(4*G1*Beq^2)*int(((h+h1-hn)^2-y^2)^2,[-hn+h -hn+h+h1]));

Keq = 1/(1/K + 1/K1);

phi = f*sqrt(rhoL*Beq)/(2*Keq);

% B_eq takes into account different material, B_K shear

B(f) = Beq/(sqrt(1+phi^2)+phi)^2;


% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = double(B(0))*ke;

% WARNING: 2 for reduced integration, 4 for complete
me = L/2*gaussIntegrate(M_N, 2);
me = rhoL*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Point load
% ----------

% nodeLoad = N;
% dofLoad  = 1;
% 
% P = 1;
% values = [nodeLoad dofLoad P]; % [node, dof, value]
% 
% F = assemblePointF(values, N*dof);

% Distributed load
% ----------------
nodes = (1:(N-1))';
values = [nodes ones(size(nodes)) b*ones(size(nodes))]; % [startNode, dof, value]
F = assembleForce(L, values, dof*N);

% WARNING: if a pressure P is applied instead of a distributed line load,
% adjust the value multiplying by width of section

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 3;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];

    case 4
    % Free
    BC = [];
   
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);
F_BC = applyBC(F, BC);

% Response
% --------

% Frequecy vector
steps = 500;
fMin  = 0;
fMax  = 10000*2*pi;

f = linspace(fMin, fMax, steps); % rad/s

% Impulse load
P = ones(steps,1);

% Precondition with unit matrix
I = eye(size(M_BC),'like', M_BC);

% Initialisation
x = zeros(length(I),steps);
x_oberst = zeros(length(I),steps);

x0 = x(:,1); 
x0_oberst = x_oberst(:,1); 

for k = 1:length(f) 
    
    s = 1i*f(k);
    Z = M_BC*s^2 + K_BC*double(B(f(k))/B(0));
    Z_oberst = M_BC*s^2 + K_BC*double(Beq(f(k))/B(0));

    F = F_BC*P(k);

    [xk,~] = bicg(Z,F,1e-6,1e6,I,I,x0); % biconjugate gradients method 
    x(:,k) = xk;
    x0 = xk; % use x0 = x_k-1

    [xk_oberst,~] = bicg(Z_oberst,F,1e-6,1e6,I,I,x0);
    x_oberst(:,k) = xk_oberst;
    x0_oberst = xk_oberst;
    
end

% Assemble x
x = assembleBC(x, BC);
x_oberst = assembleBC(x_oberst, BC);

% RMS response of all points (RMS of modulus)
x = x(1:2:end,:); % each column is the response at a certain f
rms = mean(abs(x).^2).^0.5; % mean is done in every column

x_oberst = x_oberst(1:2:end,:); % each column is the response at a certain f
rms_oberst = mean(abs(x_oberst).^2).^0.5;

figure
semilogy(f,rms, f,rms_oberst);
title('RMS ')
xlabel('$\omega$ (rad/s)'), ylabel('Amplitude (m)')
legend('New model', 'Oberst')

% Save data 
% ---------

save(['response/FLDbeam' num2str(h1*1000) 'mmB' num2str(boundary) ],'f','rms','rms_oberst')
savefig(gcf, ['figures/FLDbeam' num2str(h1*1000) 'mmB' num2str(boundary)])