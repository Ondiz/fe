% Time response of beam 
% ----------------------

% Time response of beam with fractional bearing supports in both ends

clear
close all
clc

% Set latex as interpreter
set(groot,'defaulttextinterpreter','latex');  
set(groot,'defaultAxesTickLabelInterpreter','latex');  
set(groot,'defaultLegendInterpreter','latex');

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;     % number of nodes
dof = 2;   % number of dof/node

% Simulation time

T = 1; % simulation time
samples = 1000;
Fs = samples/T;

% Material and section data

r = 0.02;         % radius of section
Lt = 1;           % length of beam
rho = 7900;       % density 
A = pi*r^2;       % area of section
E = 210E9;        % Young's modulus
I = pi/4*r^4;     % inertia

L = Lt/(N-1); % element length

% Cases
% ------

% Reference values
kref = 1e4;
cref = 5e1;

% 1) low k, low c, viscous: rigid solid mode
% 2) low k, big c, viscous: rigid solid mode + supercritical damping
% 3) low k, big c, fractional: rigid solid mode, doesn't cross zero
% 4) big k, low c, fractional: coupled rigid body + elastic

option = 4;

switch option
    
    case 1
        alpha = 1;
        k1 = kref; 
        c1 = cref;
        file = 'modes/case1.gif';
        pauseT = 0.005;
    
    case 2
        alpha = 1;
        k1 = kref; 
        c1 = 10*cref;
        file = 'modes/case2.gif';
        pauseT = 0.005;
        
    case 3
        alpha = 0.6;
        k1 = kref; 
        c1 = 100*cref;
        file = 'modes/case3.gif';
        pauseT = 0.005;

    case 4
        alpha = 0.6;
        k1 = 10*kref; 
        c1 = cref;
        file = 'modes/case4.gif';
        pauseT = 0.02;

end

k2 = k1;
c2 = c1;


% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);


% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

% WARNING: 4 integration points for full integration, 2 for reduced
% Reduced integration causes noise in time response
me = L/2*gaussIntegrate(M_N, 4);
me = rho*A*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N+1);
M = assemble(me, connect, dof*N+1);

% Support
% -------

% Matsubara
% k1 = 198.5e6; % k1 = 381.8e6; % High load
% k1 = 56e6; % k1 = 90e6; % Low load
% k2 = k1;

% Position
i = [1 length(K)-2  length(K)-2 length(K) length(K)]; % K has N+1 dofs
j = [1 length(K)-2 length(K) length(K)-2 length(K)];
v = [k1 k2 -k2 -k2 k2];

Ks = sparse(i,j,v,size(K,1),size(K,1));

K = K + Ks;

Kuu = K(1:end-1,1:end-1);
Kub = K(1:end-1,end);

Muu = M(1:end-1,1:end-1);
Mub = M(1:end-1,end);

% Damping matrix
% --------------

% Matsubara
% c = 20000 -- 200000 kg/s.
% c1 = 2e4;

% Position
i = [1 length(K)-2  length(K)-2 length(K) length(K)]; % K has N+1 dofs
j = [1 length(K)-2 length(K) length(K)-2 length(K)];
v = [c1 c2 -c2 -c2 c2];

C = sparse(i,j,v,size(K,1),size(K,1));

Cuu = C(1:end-1,1:end-1);
Cub = C(1:end-1,end);

% Boundary conditions
% --------------------

BC = [];

M_BC = applyBC(Muu, BC);
K_BC = applyBC(Kuu, BC);
C_BC = applyBC(Cuu,BC);

% Response
% -------- 

% Initial deformation
ub = 3e-3;
Kub = applyBC(Kub,BC);
X0 = -K_BC\(Kub*ub);

% Initial velocity
dotX0 = zeros(size(X0));

% Initial acceleration
ddotX0 = -M_BC\(C_BC*dotX0 + K_BC*X0);

% Newmark coefficients
gamma = 0.5;
beta = 0.25*(0.5+gamma)^2;
[X, dotX, ddotX] = newmarkIC_Frac({M_BC, C_BC, K_BC}, {X0, dotX0, ddotX0}, T, samples, [gamma,beta], alpha);

X = assembleBC(X, BC);
dotX = assembleBC(dotX, BC);
ddotX = assembleBC(ddotX, BC);

% Response in a single node and DOF
% Only linear movement
node = N;
map = (node-1)*2 + 1;

Xn = X(map,:);
dotXn = dotX(map,:);
ddotXn = ddotX(map,:);

% Displacement
fig = plot(linspace(0,T,samples),Xn);
xlabel('$t~(\mathrm{s})$'), ylabel('$u~(\mathrm{m})$')
[FFT_Data, f_FFT] = espectro(full(Xn), Fs);
figure
fig = plot(f_FFT,abs(FFT_Data));
xlabel('$f~(\mathrm{Hz})$'), ylabel('$U~(\mathrm{m})$')

% Speed
figure
fig = plot(linspace(0,T,samples),dotXn);
xlabel('$t~(\mathrm{s})$'), ylabel('$v~(\mathrm{m/s})$')

% Acc
figure
fig = plot(linspace(0,T,samples),ddotXn);
xlabel('$t~(\mathrm{s})$'), ylabel('$a~(\mathrm{m/s^2})$')

% Natural frequencies
% -------------------

% Compare peaks to natural frequencies
[phi, lambda] = eigs(K_BC,M_BC,5,'smallestabs');
lambda = sqrt(diag(lambda));
lambda = lambda/(2*pi); % Hz
    
% Animation
% ---------

% Maximum displacement 
maxX = max(max(abs(X(1:2:end,:))));

figure(5)

for i = 1:10:samples 
    figure(5) % to avoid plotting in the current figure
    fig = plot(0:L:Lt,X(1:2:end,i));
    xlabel('$L~(\mathrm{m})$'), ylabel('$u~(\mathrm{m})$')
    axis([0 Lt -3e-3 3e-3]);
    drawnow
    pause(pauseT)
    
    frame = getframe(gcf);
    img =  frame2im(frame);
    [img,cmap] = rgb2ind(img,256);
    if i == 1
        imwrite(img,cmap,file,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(img,cmap,file,'gif','WriteMode','append','DelayTime',0.1);
    end
end

% Deformation in time

figure(6)
beam = 0:L:Lt;

for i = 1:100:samples 
    fig = plot3(i/Fs*ones(size(beam)),beam, X(1:2:end,i),'LineWidth',1.2,'Color','k');
    xlabel('$t$'), ylabel('$L$'), zlabel('$u(t)$')
    hold on
    grid on
end

layout(fig, 24, 'Century Schoolbook', 12, 1.5, true, [800 700])   

% NOTES:
% - Tested alpha = 0 and alpha = 1, alpha = 1 same results as viscous case
