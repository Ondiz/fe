% Computation of response of CLD beam
% -----------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;    % number of nodes
dof = 2;   % number of dof/node


b = 0.01;         % width of section
Lt = 0.12;        % length of beam
L = Lt/(N-1);     % element length

H1 = 0.001;
% H2 = 0.001;
% H2 = 0.005;
H2 = 0.010;
H3 = H1;

rho1 = 7782;          % density 
E1   = 176.2E9;       % Young's modulus
nu   = 0.3;           % Poisson's ratio
G1   = E1/(2*(1+nu)); % shear modulus

rho2 = 1423; 

E3   = E1;
rho3 = rho1;
G3   = G1;

rhoL = (rho1*H1 + rho2*H2 + rho3*H3)*b;

% Fractional damping
% ------------------

syms f

E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

E2 = (E_0 + E_inf*(1i*tau*f).^alpha)./(1+(1i*tau*f).^alpha);
G2(f) = E2/(2*(1+nu)); 

hn = (E1*H1^2 + E2*H2*(2*H1+H2) + E3*H3*(2*H1+2*H2+H3))/(2*(E1*H1+E2*H2+E3*H3)); % neutral fiber

syms x

lim1 = -hn;
lim2 = H1 - hn;
lim3 = H1 + H2 - hn;
lim4 = H1 + H2 + H3 - hn;

I1 = b*int(x^2, lim1, lim2); % inertia moment of base material
I2 = b*int(x^2, lim2, lim3); % inertia moment of viscoelastic material 
I3 = b*int(x^2, lim3, lim4); % inertia moment of base material for CLD

Beq(f) = E1*I1 + E2*I2 + E3*I3;

omega1 = int(E1*x,x,lim1);
K1 = (G1*Beq^2/b)/int(omega1^2, lim1, lim2);
K1 = simplify(K1); 
    
omega2 = E1*H1*(hn-H1/2)+ E2*((hn-H1)^2 - x^2)/2;
K2 = (G2*Beq^2/b)/int(omega2^2, x, lim2, lim3);
K2 = simplify(K2);

omega3 = int(E3*x,x,lim4);
K3 = (G3*Beq^2/b)/int(omega3^2, x, lim3, lim4);
K3 = simplify(K3);
Keq(f) = 1/(1/K1 + 1/K2 + 1/K3);

phi(f) = f*sqrt(rhoL*Beq)/(2*Keq);

% B_eq takes into account different material, B_K shear

B(f) = Beq/(sqrt(1+phi^2)+phi)^2;


% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = double(B(0))*ke;

% WARNING: 2 for reduced integration, 4 for complete
me = L/2*gaussIntegrate(M_N, 2);
me = rhoL*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Point load
% ----------

% nodeLoad = N;
% dofLoad  = 1;
% 
% P = 1;
% values = [nodeLoad dofLoad P]; % [node, dof, value]
% 
% F = assemblePointF(values, N*dof);

% Distributed load
% ----------------
nodes = (1:(N-1))';
values = [nodes ones(size(nodes)) b*ones(size(nodes))]; % [startNode, dof, value]
F = assembleForce(L, values, dof*N);

% WARNING: if a pressure P is applied instead of a distributed line load,
% adjust the value multiplying by width of section

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 3;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];

    case 4
    % Free
    BC = [];
   
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);
F_BC = applyBC(F, BC);

% Response
% --------

% Frequecy vector
steps = 500;
fMin  = 0;
fMax  = 10000*2*pi;

f = linspace(fMin, fMax, steps); % rad/s

% Impulse load
P = ones(steps,1);

% Precondition with unit matrix
I = eye(size(M_BC),'like', M_BC);

% Initialisation
x = zeros(length(I),steps);
x0 = x(:,1); 

x_RKU = zeros(length(I),steps);
x0_RKU = x(:,1); 

% Constants
B1 = E1*b*H1^3/12; % without Steiner
B3 = E3*b*H3^3/12;

H31 = (H1+H3)/2 + H2;
S = 1/(E1*H1) + 1/(E3*H3);
Y = 12*H31^2/(S*(E1*H1^3 + E3*H3^3));

for k = 1:length(f) 
    
    s = 1i*f(k);
    F = F_BC*P(k);
    Z = M_BC*s^2 + K_BC*double(B(f(k))/B(0));

    [xk,~] = bicg(Z,F,1e-6,1e6,I,I,x0); % biconjugate gradients method 
    x(:,k) = xk;
    x0 = xk; % use x0 = x_k-1
    
    % RKU
    wavenumber = sqrt(f(k))*(rhoL/double(Beq(f(k))))^0.25;
    X = G2(f(k))*S/(wavenumber^2*H2);
    B_RKU = (B1+B3)*(1+X*Y/(1+X));
    
    Z = M_BC*s^2 + K_BC*double(B_RKU/B(0));

    [xk_RKU,~] = bicg(Z,F,1e-6,1e6,I,I,x0_RKU); % biconjugate gradients method 
    x_RKU(:,k) = xk_RKU;
    x0_RKU = xk_RKU;
    
end

% Assemble x
x     = assembleBC(x, BC);
x_RKU = assembleBC(x_RKU, BC);

% RMS response of all points (RMS of modulus)
x = x(1:2:end,:); % each column is the response at a certain f
rms = mean(abs(x).^2).^0.5; % mean is done in every column

x_RKU = x_RKU(1:2:end,:); % each column is the response at a certain f
rms_RKU = mean(abs(x_RKU).^2).^0.5; % mean is done in every column

figure
semilogy(f,rms,f,rms_RKU);
title('RMS')
xlabel('$\omega$ (rad/s)'), ylabel('Amplitude (m)')
legend('New model','RKU')

% Save data 
% ---------

save(['response/CLDbeam' num2str(H2*1000) 'mmB' num2str(boundary) ],'f','rms','rms_RKU')
savefig(gcf, ['figures/CLDbeam' num2str(H2*1000) 'mmB' num2str(boundary)])