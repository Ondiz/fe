# Beam finite elements in Matlab

This repository holds the code for computing stuff in beams using
finite elements. The static response, the modes and the dynamic
response, both in time and in frequency of the beam are computed for
different types of supports.

The methodology described in [1] is used for the computation of
natural frequencies and mode shapes of layered beams.

[1]: "Structural vibration of flexural beams with thick unconstrained layer 
damping". Cort�s and Elejabarrieta. International Journal of Solids
and Structures (2008)

## Contents

* `static.m`: computes static response of beam
* `modes.m`: obtains eigenpairs of beam
* `time.m`: computes time response of beam
* `modeEq.m`: computes eigenpairs using an equivalent modulus
* `dynamic.m`: computes FRF 
* `data.m`: material and section data
* `modesFrac.m`: computes modes of beam supported on fractional
  bearings.
* `modesFrac_paper.m`: code for the mode computations in "An analysis
  of the dynamical behaviour of systems with fractional damping for
  Mechanical Engineering applications"
* `modesFLD.m`: code to compute the modes of FLD beam
* `modesCLD.m`: code to compute the modes of CLD beam
* `responseFLD.m`: code to compute the response of FLD beam
* `responseCLD.m`: code to compute the response of FLD beam
* `staticSeismic.m`: computes static response of beam to seismic
  excitation
* `responseSeismic.m`: computes frequency response of beam supported
  on fractional bearings
* `timeFreeEnd.m`: computes time response of beam with one end clamped or
  simple supported and the other supported by a bearing
* `timeFracSeismic.m`: computes time response of beam supported on two
  fractional bearings
* `functions`: auxiliary functions
* `testing`: data for testing purposes


## TODO:

- [x] Static beam
- [x] Mode shapes
- [x] Dynamic beam
- [x] Layered beam
- [ ] Use computed interpolation functions instead of hardcoded ones
- [x] Modes for CLD and FLD beams
- [x] Response of CLD and FLD beams
- [x] Response of beam supported by fractional bearings.
