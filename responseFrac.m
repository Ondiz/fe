% Computation of response
% ------------------------
%
% Computes response of beam supported in two bearings following fractional
% models

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 20;    % number of nodes
dof = 2;   % number of dof/node

% Material and section data

r = 0.01;         % radius of section
Lt = 1.5;         % length of beam
rho = 7900;       % density 
A = pi*r^2;       % area of section
E = 210E9;        % Young's modulus
I = pi/4*r^4;     % inertia

L = Lt/(N-1);     % element length


% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

me = L/2*gaussIntegrate(M_N, 2);
me = rho*A*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Point load
% ----------

% nodeLoad = N;
% dofLoad  = 1;
% 
% P = 1;
% values = [nodeLoad dofLoad P]; % [node, dof, value]
% 
% F = assemblePointF(values, N*dof);

% Distributed load
% ----------------
nodes = (1:(N-1))';
values = [nodes ones(size(nodes)) ones(size(nodes))]; % [startNode, dof, value]
F = assembleForce(L, values, dof*N);

% WARNING: if a pressure P is applied instead of a distributed line load,
% adjust the value multiplying by width of section

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 4;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];

    case 4
    % Free
    BC = [];
   
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);
F_BC = applyBC(F, BC);

% Fractional supports
% TODO: add values

k = 10e3; % TEST
c = 10e3; % TEST
alpha = 0.5;

% BEARING
% k = 100e6; 
% c = 1.752eX;
% TODO: assign xi for alpha = 1 and change alpha

syms w
kw(w) = k + c*(1j*w)^alpha;
% kw(w) = sym(0); % TEST

% Position
i = [1 length(K_BC)-1];
j = [1 length(K_BC)-1];
v = [double(kw(0)) double(kw(0))];

Ks = sparse(i,j,v, size(K_BC,1),size(K_BC,1));

% Response
% --------

% Frequecy vector
steps = 600;
fMin  = 0;
fMax  = 10000*2*pi;

f = linspace(fMin, fMax, steps); % rad/s

% Impulse load
P = ones(steps,1);

% Precondition with unit matrix
I = eye(size(M_BC),'like', M_BC);

% Initialisation
x = zeros(length(I),steps);
x_oberst = zeros(length(I),steps);

x0 = x(:,1); 

for n = 0:length(f)-1 
    
    s = 1i*f(n+1);
    v = [double(kw(n)) double(kw(n))]; % TODO
    Ks = sparse(i,j,v, size(K_BC,1),size(K_BC,1));
    Z = M_BC*s^2 + K_BC+Ks;

    F = F_BC*P(n+1);

    [xk,~] = bicg(Z,F,1e-9,1e6,I,I,x0); % biconjugate gradients method 
    x(:,n+1) = xk;
    x0 = xk; % use x0 = x_k-1
    
end

% Assemble x
x = assembleBC(x, BC);

% RMS response of all points (RMS of modulus)
x = x(1:2:end,:); % each column is the response at a certain f
rms = mean(abs(x).^2).^0.5; % mean is done in every column

figure
semilogy(f,rms);
title('RMS ')
xlabel('\omega (rad/s)'), ylabel('Amplitude (m)')
xlim([0,max(f)])