% Time response of beam 
% ----------------------
%
% Compute time response of beam with different BC and point loads

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 200;    % number of nodes
dof = 2;   % number of dof/node

% Simulation time

T = 1; % simulation time
samples = 100;
Fs = samples/T;

% Material and section data

data
% testData

% Rayleigh damping coefficients
alphaR = 0;
betaR = 0.01;

L = Lt/(N-1); % element length

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);


% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

me = L/2*gaussIntegrate(M_N, 4);
me = rho*A*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Excitation
% ----------

% Point load

% Harmonic load
P = cos(2*pi*linspace(0,T,samples)); % load -> multiply vector

% Unitary impulse load
% P = zeros(1,samples);
% P(1) = Fs; % amplitude * deltaT = 1

% Step
% P = ones(1,samples);

node = ceil(N/2);
% node = N;
values = [node 1 1]; % {node, dof, value}

F = assemblePointF(values, dof*N);

% Boundary conditions
% --------------------

% Clamped in first node
% BC = [1 1 0; 1 2 0]; % [node dof value]

% Clamped in two ends
BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

% Simply supported in two ends
% BC = [1 1 0; N 1 0];

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);
F_BC = applyBC(F,BC);

% Response
% --------

% Damping matrix
dim = size(M_BC,1);
% C_BC = sparse(dim, dim); % with C=0 the transient doesn't disappear 
C_BC = alphaR*M_BC + betaR*K_BC;

% Newmark coefficients
delta = 0.5;
alpha = 0.25*(0.5+delta)^2;
[X, dotX, ddotX] = newmark({M_BC, C_BC, K_BC, F_BC}, T, samples, P, delta, alpha);

X = assembleBC(X, BC);
dotX = assembleBC(dotX, BC);
ddotX = assembleBC(ddotX, BC);

% Response in a single node and DOF
% Only linear movement
map = (node-1)*dof + 1;

Xn = X(map,:);
dotXn = dotX(map,:);
ddotXn = ddotX(map,:);

% Displacement
fig = plot(linspace(0,T,samples),Xn);
xlabel('t'), ylabel('x(t)')
[FFT_Data, f_FFT] = espectro(full(Xn), Fs);
figure
fig = plot(f_FFT,abs(FFT_Data));
xlabel('f'), ylabel('X(f)')

% Speed
figure
fig = plot(linspace(0,T,samples),dotXn);
xlabel('t'), ylabel('v(t)')

% Acc
figure
fig = plot(linspace(0,T,samples),ddotXn);
xlabel('t'), ylabel('a(t)')

% Natural frequencies
% -------------------

% Compare peaks to natural frequencies
[phi, lambda] = eigs(K_BC,M_BC,30,'smallestabs');
lambda = sqrt(diag(lambda));
lambda = lambda/(2*pi);
    
% Animation
% ---------

% Maximum displacement 
maxX = max(max(abs(X(1:2:end,:))));

figure(5)
for i = 1:samples 
    figure(5) % to avoid plotting in the current figure
    fig = plot(X(1:2:end,i));
    xlabel('L'), ylabel('x(t)')
    axis([1 N -2*maxX 2*maxX]);
    drawnow
    pause(0.1)
end