% Computation of mode shapes using FE
% -----------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;    % number of nodes
dof = 2;   % number of dof/node
modeN = 10; % number of modes to extract

% Material and section data

b = 0.01;         % width of section
Lt = 0.12;        % length of beam
nu = 0.3;         % Poisson's ratio

L = Lt/(N-1);     % element length

% Different materials
% -------------------

% Base material

rho1 = 7782; % density
E1 = 176.2E9; % Young's modulus
G1 = E1/(2*(1+nu)); % shear modulus
H1 = 0.002;
A1 = b*H1;

H3 = 0;
E3 = 0; % fld

% Viscoelastic material

syms f

rho2 = 1423; 
H2 = 0.002;
E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

%E2 = (E_0 + E_inf*(1i*tau*f)^alpha)/(1+(1i*tau*f)^alpha);
E2 = E1; % TEST: single material
G2 = E2/(2*(1+nu)); 
A2 = b*H2;

rhoL = rho1*A1 + rho2*A2;

hn = (E1*H1^2 + E2*H2*(2*H1+H2) + E3*H3*(2*H1+2*H2+H3))/(2*(E1*H1+E2*H2+E3*H3)); % neutral fiber

lim1 = -hn;
lim2 = H1 - hn;
lim3 = H1 + H2 - hn;
lim4 = H1 + H2 + H3 - hn; 

syms x

I1 = b*int(x^2, lim1, lim2); % inertia moment of base material
I2 = b*int(x^2, lim2, lim3); % inertia moment of viscoelastic material 
I3 = b*int(x^2, lim3, lim4); % inertia moment of base material for CLD

Beq(f) = E1*I1 + E2*I2 + E3*I3;

K1 = 1/(E1^2*b/(4*G1*Beq^2)*int((hn^2-x^2)^2,[lim1 lim2]));
K2 = 1/(E2^2*b/(4*G2*Beq^2)*int(((H1+H2-hn)^2-x^2)^2,[lim2 lim3]));

Keq = 1/(1/K1 + 1/K2);

phi(f) = f*sqrt(rhoL*Beq)/(2*Keq);

% B_eq takes into account different material, B_K shear

B(f) = Beq/(sqrt(1+phi^2)+phi)^2;

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = double(B(0))*ke;

me = L/2*gaussIntegrate(M_N, 2);
me = rhoL*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 1;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];

    case 4
    % Free
    BC = [];
   
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);

% Eigenpairs
% ----------

[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');
omega = real(sqrt(diag(lambda)));

fprintf('Natural frequencies for B(0):\n')
fprintf('%.2f\n',omega)

u = assembleBC(phi, BC);
u = u(1:2:end,:);

figure
fig = plot(0:L:Lt,u);
xlabel('x'), ylabel('u'), title('Mode shapes')

tol = 0.01;

[~, lambdaNew] = iterativeEigs(K_BC, M_BC, B, modeN, tol);
omegaNew = real(sqrt(lambdaNew));

fprintf('\nNatural frequencies with B(w):\n')
fprintf('%.2f\n',omegaNew)
