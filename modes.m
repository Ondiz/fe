% Computation of mode shapes using FE
% -----------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;    % number of nodes
dof = 2;   % number of dof/node
modeN = 5; % number of modes to extract

% Material and section data

data
% testData

L = Lt/(N-1); % element length

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

me = L/2*gaussIntegrate(M_N, 2);
me = rho*A*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 3;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]
    
    % Check
    coefs = [1.875 4.694 7.855 10.996 14.137];

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]
    
    % Check
    coefs = [4.73 7.853 10.996 14.137 17.279];

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];
    
    % Check
    coefs = (1:5)*pi;

    case 4
    % Free
    BC = [];
    
    % Check
    coefs = [4.73 7.853 10.996 14.137 17.279];
    
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);

% Eigenpairs
% ----------

[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');

lambda = real(sqrt(diag(lambda)));

u = assembleBC(phi, BC);
u = u(1:2:end,:);

figure
fig = plot(0:L:Lt,u);
xlabel('x'), ylabel('u'), title('Mode shapes')

% Computation of error
tol = 1e-5;
f_computed = lambda(lambda > tol);
f_computed = f_computed(1:5);

f_theory = coefs.^2/Lt^2*sqrt(E*I/(rho*A));
f_theory = f_theory';

error = abs(f_computed - f_theory)./f_computed;
disp('Error in natural frequencies [%]: ')
disp(error)
