% Static response of beam using FE
% --------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 4;    % number of nodes
dof = 2;   % number of dof/node

% Material and section data

data
% testData

L = Lt/(N-1); % element length

% Interpolation functions
% -----------------------

[~, K_N] = interpFunc(L);


% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

% Additional degree of freedom N+1 because a displacement is set in the
% last node:
K = assemble(ke, connect, dof*N+1);

% Supports
% --------

% - The first node is connected to the ground by a spring
% 
% - The last node is connect to the ground by a spring
% Ks = [k -k; -k k] in the positions (N-1,N-1),(N-1,N+1),(N+1,N-1),(N+1,N+1)
% where N is the number of degrees of freedom

k1 = 1e6;
k2 = 1e6;

% Position
i = [1 length(K)-2  length(K)-2 length(K) length(K)]; % K has N+1 dofs
j = [1 length(K)-2 length(K) length(K)-2 length(K)];
v = [k1 k2 -k2 -k2 k2];

Ks = sparse(i,j,v,size(K,1),size(K,1));

K = K + Ks;

% Deformation
% -----------

% Organising degrees of freedom as deformations and base movement
%
% [Kuu Kub; Kbu Kbb]*[uu ub] = [F R]

% Kuu*uu + Kub*ub = F -->  Kuu*uu = F - Kub*ub
% Kub*uu + Kbb*ub = R

% F = applied force, R = support reactions

Kuu = K(1:end-1,1:end-1);
Kub = K(1:end-1,end);
ub = 1; % unit displacement of right support
 
uu = -Kuu\(Kub*ub);

% Take only displacements
uu = uu(1:2:end-1);

fig = plot(0:L:Lt,uu);
xlabel('x'), ylabel('u'), title('Static deformation')
