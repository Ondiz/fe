% Computation of FRF using FE
% ---------------------------

% Compare to "Structural vibration of flexural beams with thick
% unconstrained layer damping"

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;    % number of nodes
dof = 2;   % number of dof/node

% Material and section data

b = 0.01;         % width of section
Lt = 0.12;        % length of beam
rho = 7782;       % density 
E = 176.2E9;      % Young's modulus
nu = 0.3;         % Poisson's ratio
G = E/(2*(1+nu)); % shear modulus

L = Lt/(N-1);     % element length


% Frequecy vector
steps = 600;
fMin  = 0;
fMax  = 10000*2*pi;

f = linspace(fMin, fMax, steps); % rad/s

% Different materials
% -------------------

% The 1 indicates viscoelastic material
h = 0.002;
h1   = 0.002;
rho1 = 1423; 
% rho1 = rho; % TEST: single material
A = b*h;
A1   = b*h1;
At   = b*(h+h1);

rhoL = rho*A + rho1*A1;

% Fractional damping
% ------------------

E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

E1 = (E_0 + E_inf*(1i*tau*f).^alpha)./(1+(1i*tau*f).^alpha);
% E1 = E*ones(size(f)); % TEST: single material
G1 = E1/(2*(1+nu)); 

hn = (E*h^2 + E1*h1*(2*h + h1))./(2*(E*h+E1*h1)); % neutral fiber
I = 1/12*b*h^3 + b*h*(hn - h/2).^2; % inertia moment of base material
I1 = 1/12*b*h1^3 + b*h1*(h+h1/2-hn).^2; % inertia moment of viscoelastic material 

Beq = E1.*I1 + E*I;

syms y

K = zeros(size(f));
K1 = zeros(size(f));

for t=1:length(f)

K(t)  = 1/(E^2*b/(4*G*Beq(t)^2)*int((hn(t)^2-y^2)^2,[-hn(t) -hn(t)+h]));
K1(t) = 1/(E1(t)^2*b/(4*G1(t)*Beq(t)^2)*int(((h+h1-hn(t))^2-y^2)^2,[-hn(t)+h -hn(t)+h+h1]));

end

Keq = 1./(1./K + 1./K1);

phi = f.*sqrt(rhoL*Beq)./(2*Keq);

% B_eq takes into account different material, B_K shear

B = Beq./(sqrt(1+phi.^2)+phi).^2;

% B = Beq; % TEST: single material

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = B(1)*ke;

me = L/2*gaussIntegrate(M_N, 2);
me = rhoL*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Point load
% ----------

% nodeLoad = N;
% dofLoad  = 1;
% 
% P = 1;
% values = [nodeLoad dofLoad P]; % [node, dof, value]
% 
% F = assemblePointF(values, N*dof);

% Distributed load
% ----------------
nodes = 1:(N-1);
values = [nodes ones(size(nodes)) ones(size(nodes))]; % [startNode, dof, value]
F = assembleForce(L, values, dof*N);

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 1;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];

    case 4
    % Free
    BC = [];
   
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);
F_BC = applyBC(F, BC);

% Response
% --------

nodeResp = N;
dofResp  = 1;

mapResp = (nodeResp-1)*2 + dofResp;

% Unit vector with 1 where the response is computed
e = sparse(mapResp,1,1,N*dof,1);
e = applyBC(e, BC);

resp = zeros(steps,1);

x = zeros(steps,1);

% Load variation in frequency

% Impulse load
P = ones(steps,1);

% Compute response in point 

for t = 1:steps
   
    s = 1i*f(t);
    Z = M_BC*s^2 + double(B(t)/B(1))*K_BC;
    
    h = Z\e; % only one column of H needed
    
    x(t) = h'*F_BC*P(t);
    
end

figure(2)

subplot(211), semilogy(f,abs(x)) 
% title({'FRF', ['Input node: ' num2str(nodeLoad)], [' Output node: ' num2str(nodeResp)]})
xlabel('\omega [rad/s]'), ylabel('Amplitude [m]')
hold on

subplot(212), plot(f,angle(x))
xlabel('\omega [rad/s]'), ylabel('Angle [rad]')
hold on