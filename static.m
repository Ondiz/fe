% Static response of beam using FE
% --------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 5;    % number of nodes
dof = 2;   % number of dof/node

% Material and section data

data
% testData

L = Lt/(N-1); % element length

% Interpolation functions
% -----------------------

[~, K_N] = interpFunc(L);


% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);

% Load
% -----

% Point load
% P = -1;
% values = [10 1 P]; % [node, dof, value]

% F = assemblePointF(values, dof*N);

% Distributed load in element
values = [1 1 1; 2 1 1; 3 1 1; 4 1 1]; % [startNode, dof, value]

F = assembleForce(L, values, dof*N);

% Boundary conditions
% -------------------

% Clamped in end node
% BC = [N 1 0; N 2 0]; % [node dof value]

% Clamped in start node
BC = [1 1 0; 1 2 0];

% Theoretical maximum displacement for cantilever beam
% delta = P*Lt^3/(3*E*I); % point load
% delta = P*Lt^4/(8*E*I); % uniform load in whole beam

% Clamped in both ends
% BC = [1 1 0; 1 2 0; N 1 0; N 2 0];

% Simply supported in both ends
% BC = [1 1 0; N 1 0];

% Theoretical maximum displacement for simply supported beam
% delta = P*Lt^3/(48*E*I);

K_BC = applyBC(K, BC);
F_BC = applyBC(F,BC);

uStatic = K_BC\F_BC;

% Assemble BC
uStatic = assembleBC(uStatic, BC);

% Take only displacements
uStatic = uStatic(1:2:end);

fig = plot(0:L:Lt,uStatic);
xlabel('x'), ylabel('u'), title('Static')
