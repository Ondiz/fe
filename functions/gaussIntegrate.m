function B = gaussIntegrate(A,points)
% Integrates A matrix of symbolic entries in the interval [-1 1] using the
% amount of points given. Return numeric value.
%
% B = GAUSSINTEGRATE(A,points)
%
% where:
% - A: symbolic function or matrix of functions to integrate
% - points: number of points to use

syms x

% Compute roots of Legendre polynomial of degree 'points'
coef = coeffs(legendreP(points,x),'All'); 
p = roots(coef);

% Obtain weights
d = diff(legendreP(points,x));
w = 2./((1-p.^2).*subs(d,p).^2); 

B = 0;

for i = 1:length(p)

    B = B + w(i)*subs(A,p(i));

end

% Return numeric value
B = double(B);

end