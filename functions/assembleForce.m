function F = assembleForce(L, values, N)
% Assembles force vector from computed elementary force vector. 
%
% F = ASSEMBLEFORCE(L, values, N)
%
% where:
% - L: size of the elements
% - values: [node, dof, value] triplets
% - N: total size of the problem
%
% See also ASSEMBLEPOINTF, ASSEMBLE

syms x xi

N1 = 1/4*(1-xi)^2*(2+xi);
N2 = L/8*(1-xi)^2*(1+xi);
N3 = 1/4*(1+xi)^2*(2-xi);
N4 = L/8*(1+xi)^2*(xi-1);

Ni = [N1 N2 N3 N4];

F = sparse(N, 1);

node = double(values(:,1));
dof  = double(values(:,2));
load = values(:,3);

% Dof mapping
map = (node-1)*2 + dof;

% For each element
for t=1:size(map,1)
    
    % Create elementary matrix
    Px = subs(load(t),x, (xi+1)*L/2); % variable change
    F_N = Px*Ni;
    fe = L/2*gaussIntegrate(F_N, 2)';
   
    % Positions 
    if dof(t) == 1
    
        pos = map(t) + [0 1 2 3]';
 
    elseif dof(t) == 2
        
        pos = map(t) + [-1 0 1 2]'; 
    end
    
    F = F + sparse(pos, ones(size(pos,1),1), fe(:), N, 1);
end

end