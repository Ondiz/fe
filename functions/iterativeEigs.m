function [phi, lambda] = iterativeEigs(K, M, D, modeN, tol)
% Computes eigenpairs iteratively for a dynamic system with a frequency 
% dependent stiffness matrix.
% 
% [phi, lambda] = ITERATIVEEIGS(K, M, D, modeN,tol)
%
% where:
% - K, M: system matrices
% - D: frequency dependent equivalent modulus, symbolic function
% - modeN: number of modes to compute
% - tol: tolerance

[mode, lambda] = eigs(K,M,modeN,'smallestabs');
omega = real(sqrt(diag(lambda)));

phi = zeros(length(K), modeN);
lambda = zeros(modeN,1);

for r = 1:modeN
    
    % Initialisation
    deltaOmega = tol*10;

    while deltaOmega > tol
        
        [mode,lambdaNew] = eigs(K*double(D(omega(r))/D(0)), M,modeN,'smallestabs');
        deltaOmega = abs(real(sqrt(lambdaNew(r,r)))-omega(r));
        omega(r) = real(sqrt(lambdaNew(r,r)));
        
    end
    lambda(r) = lambdaNew(r,r);
    phi(:,r)  = mode(:,r); 
    
end