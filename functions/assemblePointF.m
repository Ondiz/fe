function F = assemblePointF(values, N)
% Assembles force vector given the values of nodal forces
%
% F = ASSEMBLEPOINTF(values, N)
%
% where:
% - values: [node, dof, value] triplets
% - N: total size of the problem
%
% See also ASSEMBLEFORCE, ASSEMBLE

node = values(:,1);
dof  = values(:,2);
load = values(:,3);

% Dof mapping
map = (node-1)*2 + dof;

F = sparse(map,ones(size(map,1),1),load,N,1);

end