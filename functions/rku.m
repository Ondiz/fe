function [phi, lambda] = rku(K, M, E1, E2, E3, nu, rhoL, Bzero, sizes, modeN, tol)
% Computes eigenpairs iteratively for a dynamic system with a frequency 
% dependent stiffness matrix using RKU method.
% 
% [phi, lambda] = RKU(K, M, E1, E2, E3, nu, rhoL, Bzero, sizes, modeN, tol)
%
% Input:
% - K, M: system matrices
% - E1, E2, E3, nu: constants
% - rhoL: mass per unit length
% - Bzero: value un B at zero frequency
% - sizes: [H1 H2 H3 b] thickness of layers and width of the section
% - modeN: number of modes to compute
% - tol: tolerance
%
% 1: host, 2: visco, 3: constrained

syms f

H1 = sizes(1);
H2 = sizes(2);
H3 = sizes(3);
b  = sizes(4);

G2 = E2/(2*(1+nu));

% Bending stiffnesses
B1 = E1*b*H1^3/12;
B3 = E3*b*H3^3/12;

% Eigenvalues when B(0)
[~, lambda] = eigs(K,M,modeN,'smallestabs');
omega = real(sqrt(diag(lambda)));
omega = omega(omega > 1); % For free case

% Constants
H31 = (H1+H3)/2 + H2;
S = 1/(E1*H1) + 1/(E3*H3);
Y = 12*H31^2/(S*(E1*H1^3 + E3*H3^3));

% Initialisation
lambda = zeros(modeN,1);

for r = 1:length(omega)

    % Initialisation
    deltaOmega = tol*10;
    k = sqrt(omega(r))*(rhoL/Bzero)^0.25; % Bending wave number

        while deltaOmega > tol
            
            X = G2*S/(k^2*H2);
            Beq(f) = (B1+B3)*(1+X*Y/(1+X));
            
            % Check if Beq is symbolic
            if isa(Beq,'sym')
                [phi,lambdaNew] = eigs(K*double(Beq(omega(r))/Bzero), M,modeN,'smallestabs'); 
                k = sqrt(omega(r))*(rhoL/double(Beq(omega(r))))^0.25;
            else
                [phi,lambdaNew] = eigs(K*double(Beq/Bzero), M,modeN,'smallestabs');
                k = sqrt(omega(r))*(rhoL/Beq)^0.25;
            end
            deltaOmega = abs(real(sqrt(lambdaNew(r,r)))-omega(r));
            % Update
            omega(r) = real(sqrt(lambdaNew(r,r)));
        end

    lambda(r) = lambdaNew(r,r);
    
end