function N = interpolate(L)
% Computes interpolation functions for thin beam given the size L of the
% element
%
% N = INTERPOLATE(L)
%
% N is organised as N = [N1 N3 N2 N4], first the interpolation functions of
% the displacement and after the rotations.

syms x

% Terms and coefficients
term = [1 x x^2 x^3];
var = length(term); % number of variables
coef = sym('a',[1,var]); % creates a1, a2, ...

% Vertical displacement
% w = a1+a2*x+a3*x^2+a4*x^3;
w(x) = coef*term.';

% Rotations
theta(x) = diff(w,x);

% Nodes of the element
nodes = [-1; 1]';

X = nodes(1,:);

% Sustitución en los nodos
a = [w(X) theta(X)];

% Initialisation
A = zeros(var,var);
v = 1:var;

for k = 1:var

[c,t] = coeffs(a(k), coef);
n = ismember(coef,t); 
A(k,nonzeros(n.*v)) = c; % nonzeros(n.*v) finds the position of values of c in matrix A

end

N = term/A;

% The rotations have to be multiplied by half the length of the element 
N(3:4) = L/2*N(3:4);

end