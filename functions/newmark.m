function [X, dotX, ddotX] = newmark(matrices, T, samples, P, delta, alpha)
% Computes time response using Newmark method
%
% [X, dotX, ddotX] = NEWMARK(matrices, T, P, delta, alpha)
%
% Input:
% - matrices: {M_BC, C_BC, K_BC, F_BC}
% - T: period of time
% - samples: number of samples
% - P: load vector in time
% - delta, alpha: Newmark constants
%
% Output:
% - X: nodal displacementes in time (N*dof-BC) x samples
% - dotX: nodal speeds in time (N*dof-BC) x samples
% - ddotX: nodal accelerations in time (N*dof-BC) x samples

M_BC = matrices{1};
C_BC = matrices{2};
K_BC = matrices{3};
F_BC = matrices{4};

dim = size(M_BC,1);

% Parameters
deltaT = T/samples;

% Initialisation

X = sparse(dim,samples);
dotX = sparse(dim,samples);
ddotX = sparse(dim,samples);
F1 = P(1)*F_BC;
ddotX = ddotX + sparse(1:dim, ones(dim,1), M_BC\F1,dim,samples);

% Constants

a0 = 1/(alpha*deltaT^2);
a1 = delta/(alpha*deltaT);
a2 = 1/(alpha*deltaT);
a3 = 1/(2*alpha) - 1;
a4 = delta/alpha -1;
a5 = deltaT/2*(delta/alpha-2);
a6 = deltaT*(1-delta);
a7 = delta*deltaT;

% Effective stiffness matrix
Kef = K_BC + a0*M_BC + a1*C_BC;

% For each time step

for i = 1:samples-1

Ft = P(i+1)*F_BC;
Fef = Ft + M_BC*(a0*X(:,i) + a2*dotX(:,i) + a3*ddotX(:,i)) + C_BC*(a1*X(:,i) + a4*dotX(:,i) + a5*ddotX(:,i));

X = X + sparse(1:dim, (i+1)*ones(dim,1), Kef\Fef,dim,samples);

vv = a0*(X(:,i+1) - X(:,i)) - a2*dotX(:,i) - a3*ddotX(:,i);
ddotX = ddotX + sparse(1:dim, (i+1)*ones(dim,1), vv, dim,samples);

v = dotX(:,i) + a6*ddotX(:,i) + a7*ddotX(:,i+1);
dotX = dotX + sparse(1:dim, (i+1)*ones(dim,1), v, dim,samples);

end



