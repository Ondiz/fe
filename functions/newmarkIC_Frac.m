function [X, dotX, ddotX] = newmarkIC_Frac(matrices, IC, T, samples, newmarkPar, alpha)
% Computes time response of a system with fractional damping using Newmark
% method given the initial conditions
%
% [X, dotX, ddotX] = newmarkIC_Frac(matrices, IC, T, samples, newmarkPar, alpha)
%
% Input:
% - matrices: {M_BC, C_BC, K_BC}
% - IC: initial conditions {X0, dotX0, ddotX0}
% - T: period of time
% - samples: number of samples
% - newmarkPar: Newmark constants [gamma, beta]
% - alpha: order of the fractional derivative
%
% Output:
% - X: nodal displacementes in time (N*dof-BC) x samples
% - dotX: nodal speeds in time (N*dof-BC) x samples
% - ddotX: nodal accelerations in time (N*dof-BC) x samples

gamma = newmarkPar(1);
beta = newmarkPar(2);

M_BC = matrices{1};
C_BC = matrices{2};
K_BC = matrices{3};

dim = size(M_BC,1);

% Initial conditions
X0 = IC{1}; 
dotX0 = IC{2}; 
ddotX0 = IC{3}; 

% Parameters
deltaT = T/samples;

% Initialisation

X = sparse(dim,samples);
dotX = sparse(dim,samples);
ddotX = sparse(dim,samples);

X = X + sparse(1:dim, ones(dim,1), X0,dim,samples);
dotX = dotX + sparse(1:dim, ones(dim,1), dotX0,dim,samples);
ddotX = ddotX + sparse(1:dim, ones(dim,1), ddotX0,dim,samples);

% Constants

a0 = 1/(beta*deltaT^2);
a1 = gamma/(beta*deltaT);
a2 = 1/(beta*deltaT);
a3 = 1/(2*beta) - 1;
a4 = gamma/beta -1;
a5 = deltaT/2*(gamma/beta-2);
a6 = deltaT*(1-gamma);
a7 = gamma*deltaT;

% Effective stiffness matrix
Kef = K_BC + a0*M_BC + a1*deltaT^(1-alpha)*C_BC;

% Coefficients for G1
A = zeros(1,samples);
A(1) = 1;

for j = 1:(samples-1)
    A(j+1) = (j-alpha-1)/j*A(j); 
end


% For each time step
for i = 1:samples-1

Fef = M_BC*(a0*X(:,i) + a2*dotX(:,i) + a3*ddotX(:,i)) + C_BC*deltaT^(1-alpha)*(a1*X(:,i) + a4*dotX(:,i) + a5*ddotX(:,i));
Fef = Fef - 1/deltaT^alpha*C_BC*(X(:,i) + sum(A(2:size(X(:,1:i),2)+1).*flip(X(:,1:i),2),2));

X = X + sparse(1:dim, (i+1)*ones(dim,1), Kef\Fef,dim,samples);

vv = a0*(X(:,i+1) - X(:,i)) - a2*dotX(:,i) - a3*ddotX(:,i);
ddotX = ddotX + sparse(1:dim, (i+1)*ones(dim,1), vv, dim,samples);

v = dotX(:,i) + a6*ddotX(:,i) + a7*ddotX(:,i+1);
dotX = dotX + sparse(1:dim, (i+1)*ones(dim,1), v, dim,samples);

end



