function [M_N, K_N] = interpFunc(L)
% Given the length of the element produces interpolation functions for
% the creation of element matrices Me and Ke 
%
% [M_N, K_N] = ELEMMATRICES(L)
%
% where:
% - L: length of element
% - M_N, K_N: symbolic element matrices for integration 

% Interpolation function are defined elementwise in the domain [-1 1]

syms xi x

N1 = 1/4*(1-xi)^2*(2+xi);
N2 = L/8*(1-xi)^2*(1+xi);
N3 = 1/4*(1+xi)^2*(2-xi);
N4 = L/8*(1+xi)^2*(xi-1);

Ni = [N1 N2 N3 N4];

xi = 2*x/L -1; 

% Derivatives
% Chain rule

Nixx = diff(Ni,2)*jacobian(xi,x)^2;

% Stiffness matrix
K_N = Nixx.'*Nixx;

% Mass matrix
M_N = Ni.'*Ni;

end