% Computation of mode shapes using FE
% -----------------------------------
%
% Computes modes of beam supported in two bearings following fractional
% models

clear
close all
clc

% Set latex as interpreter
set(groot,'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;    % number of nodes
dof = 2;   % number of dof/node
modeN = 5; % number of modes to extract

% Material and section data

r = 0.02;         % radius of section
Lt = 1;           % length of beam
rho = 7900;       % density 
A = pi*r^2;       % area of section
E = 210E9;        % Young's modulus
I = pi/4*r^4;     % inertia

L = Lt/(N-1); % element length

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

me = L/2*gaussIntegrate(M_N, 4);
me = rho*A*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 4;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];
    
    case 4
    % Free
    BC = [];
    
    
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);


% BEARING
k = 1e4;
c = 5e1*20;
alpha = 1;

syms w
kw(w) = k + c*(1j*w)^alpha;
% kw(w) = sym(0); % TEST

% Position
i = [1 length(K_BC)-1];
j = [1 length(K_BC)-1];
v = [double(kw(0)) double(kw(0))];

Ks = sparse(i,j,v, size(K_BC,1),size(K_BC,1));


% Eigenpairs
% ----------

K = K_BC + Ks;

[phi, lambda] = eigs(K,M_BC,modeN,'smallestabs');

omega = real(sqrt(diag(lambda)));

fprintf('Natural frequencies for w=0:\n')
fprintf('%.2f\n',omega)

u0 = assembleBC(phi, BC);
u0 = u0(1:2:end,:);

figure
fig = plot(0:L:Lt,u0);
xlabel('x'), ylabel('u'), title('Mode shapes')

% Frequency dependence
tol = 0.01;

phi = zeros(length(K), modeN);
lambda = zeros(modeN,1);

for r = 1:modeN
    
    % Initialisation
    deltaOmega = tol*10;

    while deltaOmega > tol
       
        v = [double(kw(omega(r))) double(kw(omega(r)))];

        Ks = sparse(i,j,v, size(K_BC,1),size(K_BC,1));
        
        [mode,lambdaNew] = eigs(K_BC + Ks, M, modeN,'smallestabs');
        deltaOmega = abs(real(sqrt(lambdaNew(r,r)))-omega(r));
        omega(r) = real(sqrt(lambdaNew(r,r)));
        
    end
    lambda(r) = lambdaNew(r,r);
    phi(:,r)  = mode(:,r); 
    
end

fprintf('\nNatural frequencies with frequency dependent support:\n')
fprintf('%.2f\n',omega)

u = assembleBC(phi, BC);
u = u(1:2:end,:);

figure
subplot(211)
fig = plot(0:L:Lt,real(u));
xlabel('$L$ (m)'), ylabel('Mode shape'), title('Mode shapes with frequency dependent support')

subplot(212)
fig = plot(0:L:Lt,angle(u));
ylim([-pi pi])
xlabel('$L$ (m)'), ylabel('Phase~(rad)')

% Animation of mode

mode = 5;
file = ['modes/fracsupport' num2str(mode) '.gif'];

figure(5)
for t = 0:0.1:10*pi
    
    figure(5)
    plot(0:L:Lt,real(u(:,mode)*exp(1j*t)));
    axis([0 Lt -1 1])
    axis manual
    xlabel('$L$ (m)'), ylabel('$u$ (m)')
    drawnow
    
    frame = getframe(gcf);
    img =  frame2im(frame);
    [img,cmap] = rgb2ind(img,256);
    if t == 0
        imwrite(img,cmap,file,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(img,cmap,file,'gif','WriteMode','append','DelayTime',0.1);
    end
end

% Mode deformation as surface
subplot(121)
t = linspace(0,2*pi,60);
x = 0:L:Lt;

[X,T] = meshgrid(x,t);

Z = real(u(:,mode)*exp(1j*t));

colormap(bone)
surf(X,T,Z')
xlabel('Length (m)'), ylabel('Period')
hold on
surf(X,T,zeros(size(X)),'FaceAlpha',0.5,'EdgeColor','none')

% Compare to real mode
subplot(122)
Z = real(u0(:,mode)*exp(1j*t));

surf(X,T,Z')
xlabel('Length (m)'), ylabel('Period')

hold on
surf(X,T,zeros(size(X)),'FaceAlpha',0.5,'EdgeColor','none')

% NOTE
%
% - Need big c to affect mode shapes -- frequencies
% - Mode in time 3D