% Computation of mode shapes using FE for FLD plate
% -------------------------------------------------

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 60;     % number of nodes
dof = 2;    % number of dof/node
modeN = 10; % number of modes to extract

% Material and section data

b = 0.01;         % width of section
Lt = 0.12;        % length of beam
nu = 0.3;         % Poisson's ratio

L = Lt/(N-1);     % element length

% Different materials
% -------------------

% Base material

rho1 = 7782; % density
E1 = 176.2E9; % Young's modulus
G1 = E1/(2*(1+nu)); % shear modulus
H1 = 0.001;
A1 = b*H1;

H3 = H1;
E3 = E1; % cld
G3 = G1;

% Viscoelastic material

H2 = 0.001;
% H2 = 0.005;
% H2 = 0.010;

syms f

rho2 = 1423; 
E_0   = 0.353E9;
E_inf = 3.462E9;
tau   = 314.9E-6;
alpha = 0.873;

A3 = b*H3;
rho3 = rho1;

E2 = (E_0 + E_inf*(1i*tau*f)^alpha)/(1+(1i*tau*f)^alpha);
G2 = E2/(2*(1+nu)); 
A2 = b*H2;

rhoL = rho1*A1 + rho2*A2 + rho3*A3;

hn = (E1*H1^2 + E2*H2*(2*H1+H2) + E3*H3*(2*H1+2*H2+H3))/(2*(E1*H1+E2*H2+E3*H3)); % neutral fiber

lim1 = -hn;
lim2 = H1 - hn;
lim3 = H1 + H2 - hn;
lim4 = H1 + H2 + H3 - hn; 

syms x

I1 = b*int(x^2, lim1, lim2); % inertia moment of base material
I2 = b*int(x^2, lim2, lim3); % inertia moment of viscoelastic material 
I3 = b*int(x^2, lim3, lim4); % inertia moment of base material for CLD

Beq(f) = E1*I1 + E2*I2 + E3*I3;

omega1 = int(E1*x,x,lim1);
K1 = (G1*Beq^2/b)/int(omega1^2, lim1, lim2);
K1 = simplify(K1); 
    
omega2 = E1*H1*(hn-H1/2)+ E2*((hn-H1)^2 - x^2)/2;
K2 = (G2*Beq^2/b)/int(omega2^2, x, lim2, lim3);
K2 = simplify(K2);

omega3 = int(E3*x,x,lim4);
K3 = (G3*Beq^2/b)/int(omega3^2, x, lim3, lim4);
K3 = simplify(K3);
Keq(f) = 1/(1/K1 + 1/K2 + 1/K3);

phi(f) = f*sqrt(rhoL*Beq)/(2*Keq);

% B_eq takes into account different material, B_K shear

B(f) = Beq/(sqrt(1+phi^2)+phi)^2;

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = double(B(0))*ke;

me = L/2*gaussIntegrate(M_N, 2);
me = rhoL*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N);
M = assemble(me, connect, dof*N);

% Boundary conditions
% --------------------

% Case 1: cantilever
% Case 2: clamped in the two ends
% Case 3: simply supported in the two ends
% Case 4: free

boundary = 3;

switch boundary
    
    case 1
    % Clamped in first end
    BC = [1 1 0; 1 2 0]; % [node dof value]

    case 2
    % Clamped in two ends
    BC = [1 1 0; 1 2 0; N 1 0; N 2 0]; % [node dof value]

    case 3
    % Simply supported in two ends
    BC = [1 1 0; N 1 0];

    case 4
    % Free
    BC = [];
   
end

M_BC = applyBC(M, BC);
K_BC = applyBC(K, BC);

% Eigenpairs
% ----------

[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');
omega = real(sqrt(diag(lambda)));

fprintf('Natural frequencies for B(0):\n')
fprintf('%.2f\n',omega)

u = assembleBC(phi, BC);
u = u(1:2:end,:);

figure
fig = plot(0:L:Lt,u);
xlabel('x'), ylabel('u'), title('Mode shapes')

tol = 0.01;

[phiNew, lambdaNew] = iterativeEigs(K_BC, M_BC, B, modeN, tol);
omegaNew = real(sqrt(lambdaNew));

uNew = assembleBC(phiNew, BC);
uNew = uNew(1:2:end,:); % each mode is a column

fprintf('\nNatural frequencies with B(w):\n')
fprintf('%.2f\n',omegaNew)

% RKU
[phiRKU, lambdaRKU] = rku(K_BC, M_BC, E1, E2, E3, nu, rhoL, double(B(0)), [H1 H2 H3 b], modeN, tol);
omegaRKU = real(sqrt(lambdaRKU));

uRKU = assembleBC(phiRKU, BC);
uRKU = uRKU(1:2:end,:); % each mode is a column

fprintf('\nNatural frequencies (RKU):\n')
fprintf('%.2f\n',omegaRKU)

% Effect of shear in damping
% 
% lambda = omega^2*(1 + i*eta)

etaNew = imag(lambdaNew)./real(lambdaNew);
fprintf('\nDamping ratio taking shear and damping into account\n')
fprintf('%.4f\n',etaNew)

etaRKU = imag(lambdaRKU)./real(lambdaRKU);
fprintf('\nDamping ratio for RKU\n')
fprintf('%.4f\n',etaRKU)

% Export modes for MAC
% ---------------------

x = 0:L:Lt; % coordinates
save(['modes/modesCLD' num2str(H2*1000) 'mmB' num2str(boundary)],'x','omegaNew','omegaRKU','uNew','uRKU')